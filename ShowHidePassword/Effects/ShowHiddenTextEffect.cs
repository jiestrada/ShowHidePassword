﻿using System;
using Xamarin.Forms;

namespace ShowHidePassword.Effects
{
    public class ShowHiddenTextEffect:RoutingEffect
    {
        public ShowHiddenTextEffect() : base("Effects.ShowHiddenEntryEffect")
        {
        }
    }
}
