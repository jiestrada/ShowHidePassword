﻿using System;
using Xamarin.Forms;

namespace ShowHidePassword.Renders
{
    public class ShowHidePasswordText:Entry
    {
        public ShowHidePasswordText()
        {
        }
        public string EntryText { get; set; }
    }
}
