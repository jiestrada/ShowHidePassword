﻿using System;
using System.Collections.Generic;
using ShowHidePassword.ViewModels;
using Xamarin.Forms;

namespace ShowHidePassword.Views
{
    public partial class ShowHidePasswordView : ContentPage
    {
        LoginViewModel ViewModel;
        public ShowHidePasswordView()
        {
            InitializeComponent();
            ViewModel = new LoginViewModel();
            BindingContext = ViewModel;

        }
    }
}
